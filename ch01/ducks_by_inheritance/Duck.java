
/**
 * Write a description of class Duck here.
 * 
 */
public class Duck
{
    /**
     * An example of a method - replace this comment with your own
     */
    public void quack()
    {
        // put your code here
        System.out.println("I am quacking...");
    }
    
    public void swim()
    {
        //
        System.out.println("I am swimming...");
    }
    
    public void display()
    {
        //
        System.out.println("The duck is displayed...");
    }
    
    public void fly()
    {
        //
        System.out.println("I am flying...");
    }
    
    // OTHER duck-like methods
    
}

