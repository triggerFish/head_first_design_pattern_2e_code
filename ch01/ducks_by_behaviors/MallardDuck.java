
/**
 * Write a description of class MallardDuck here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class MallardDuck extends Duck
{
    /**
     * Constructor for objects of class MallardDuck
     */
    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    /**
     * An example of a method - replace this comment with your own
     *
     */
    public void display() {
        System.out.println("I'm a real Mallard duck");
    }
    
}
