
/**
 * Write a description of class RedheadDuck here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RedheadDuck extends Duck implements Flyable,Quackable
{
    // instance variables - replace the example below with your own
     /**
     * Constructor for objects of class RedheadDuck
     */
    public RedheadDuck()
    {
        // initialise instance variables

    }

    public void fly()
    {
        //
        System.out.println("I am flying...");
    }
    
    public void quack()
    {
        // put your code here
        System.out.println("I am quacking...");
    }
}
