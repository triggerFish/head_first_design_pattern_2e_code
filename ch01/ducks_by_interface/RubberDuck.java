
/**
 * Write a description of class RubberDuck here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class RubberDuck extends Duck implements Quackable
{
    // instance variables - replace the example below with your own

    /**
     * Constructor for objects of class RubberDuck
     */
    public RubberDuck()
    {
        // initialise instance variables

    }

    public void quack()
    {
        // put your code here
        System.out.println("I am quacking...");
    }
}
