
/**
 * Write a description of class MuteQuack here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class MuteQuack implements QuackBehavior
{

    /**
     * An example of a method - replace this comment with your own
     *
     */
    public void quack()
    {
        System.out.print("MuteQuack << Silence >>");
    }
}
