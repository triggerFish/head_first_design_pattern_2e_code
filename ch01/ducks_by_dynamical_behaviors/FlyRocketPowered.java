
/**
 * 在这里给出对类 FlyRocketPowered 的描述。
 * 
 * @author (你的名字)
 * @version (一个版本号或者一个日期)
 */
public class FlyRocketPowered implements FlyBehavior {
    public void fly() {
        System.out.println("I'm flying with a rocket!");
    }
}
