
/**
 * 在这里给出对类 ModelDuck 的描述。
 * 
 * @author (你的名字)
 * @version (一个版本号或者一个日期)
 */
public class ModelDuck extends Duck {
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }
    
    public void display() {
        System.out.println("I'm a model duck");
    }
    
}
